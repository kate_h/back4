<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();

  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
  }

$errors = array();

$errors['lastname'] = !empty($_COOKIE['lastname_error']);
if ($errors['lastname']) {
  setcookie('lastname_error', '', 100000);
  $messages[] = '<div class="error"> Введите фамилию.</div>';
}

$errors['firstname'] = !empty($_COOKIE['firstname_error']);
if ($errors['firstname']) {
  setcookie('firstname_error', '', 100000);
  $messages[] = '<div class="error"> Введите имя.</div>';
}

$errors['email'] = !empty($_COOKIE['email_error']);
if ($errors['email']) {
  setcookie('email_error', '', 100000);
  $messages[] = '<div class="error"> Введите email.</div>';
}

$errors['date'] = !empty($_COOKIE['date_error']);
if ($errors['date']) {
  setcookie('date_error', '', 100000);
  $messages[] = '<div class="error"> Введите дату.</div>';
}

$errors['sex'] = !empty($_COOKIE['sex_error']);
if ($errors['sex']) {
  setcookie('sex_error', '', 100000);
  $messages[] = '<div class="error"> Введите пол.</div>';
}

$errors['kolvo'] = !empty($_COOKIE['kolvo_error']);
if ($errors['kolvo']) {
  setcookie('kolvo_error', '', 100000);
  $messages[] = '<div class="error"> Введите количество конечностей.</div>';
}

$errors['superpower'] = !empty($_COOKIE['superpower_error']);
if ($errors['superpower']) {
  setcookie('superpower_error', '', 100000);
  $messages[] = '<div class="error"> Введите суперспособность. </div>';
}

$errors['comment'] = !empty($_COOKIE['comment_error']);
if ($errors['comment']) {
  setcookie('comment_error', '', 100000);
  $messages[] = '<div class="error"> Введите биографию.</div>';
}

$errors['confirm'] = !empty($_COOKIE['confirm_error']);
if ($errors['confirm']) {
  setcookie('confirm_error', '', 100000);
  $messages[] = '<div class="error"> Поставьте галочку, что вы согласны с обработкой данных.</div>';
}

$errors['1'] = !empty($_COOKIE['1_error']);
if ($errors['1']){
  setcookie('1_error', '', 100000);
  $messages[] = '<div class="error">Имя и фамилия должны содержать быквы латинского алфавита без добавления лишних знаков</div>';
} 
$errors['2'] = !empty($_COOKIE['2_error']);
if ($errors['2']){
  setcookie('2_error', '', 100000);
  $messages[] = '<div class="error">Формат эл почты имеет вид: example@email.com</div>';
}
$errors['3'] = !empty($_COOKIE['3_error']);
if ($errors['3']){
  setcookie('3_error', '', 100000);
  $messages[] = '<div class="error">Формат даты 01.01.2021</div>';
} 

$values = array();
$values['lastname'] = empty($_COOKIE['lastname_value']) ? '' : $_COOKIE['lastname_value']; 
$values['firstname'] = empty($_COOKIE['firstname_value']) ? '' : $_COOKIE['firstname_value']; 
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['kolvo'] = empty($_COOKIE['kolvo_value']) ? '' : $_COOKIE['kolvo_value'];
$values['superpower'] = empty($_COOKIE['superpower_value']) ? '' : $_COOKIE['superpower_value'];
$values['comment'] = empty($_COOKIE['comment_value']) ? '' : $_COOKIE['comment_value'];
$values['confirm'] = empty($_COOKIE['confirm_value']) ? '' : $_COOKIE['confirm_value'];

include('form.php');

}

else {
  $errors = FALSE;
  if (!preg_match("/^[-a-zA-Z]+$/",$_POST['lastname'])){
    setcookie('1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (!preg_match("/^[-a-zA-Z]+$/",$_POST['firstname'])){
    setcookie('1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (!preg_match("/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/", $_POST['date'])){
    setcookie('3_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/",$_POST['email'])){
    setcookie('2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

  if (empty($_POST['lastname'])) {
    setcookie('lastname_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('lastname_value', $_POST['lastname'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['firstname'])) {
    setcookie('firstname_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('firstname_value', $_POST['firstname'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['date'])) {
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('date_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['sex'])) {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['kolvo'])) {
    setcookie('kolvo_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('kolvo_value', $_POST['kolvo'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['superpower'])) {
    setcookie('superpower_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('superpower_value', $_POST['superpower'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['comment'])) {
    setcookie('comment_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('comment_value', $_POST['comment'], time() + 365 * 24 * 60 * 60);
  }

  if (empty($_POST['confirm'])) {
    setcookie('confirm_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('confirm_value', $_POST['confirm'], time() + 365 * 24 * 60 * 60);
  }


  if ($errors) {
    header('Location: index.php');
    exit();
  }

  else {
    setcookie('1_error', '', 100000);
    setcookie('2_error', '', 100000);
    setcookie('3_error', '', 100000);
    setcookie('lastname_error', '', 100000);
    setcookie('firstname_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('kolvo_error', '', 100000);
    setcookie('superpower_error', '', 100000);
    setcookie('comment_error', '', 100000);
    setcookie('confirm_error', '', 100000);
  }


$lastname=$_POST['lastname'];
$name=$_POST['firstname'];
$email=$_POST['email'];
$date=$_POST['date'];
$sex=$_POST['sex'];
$kolvo=$_POST['kolvo'];
$superpower=$_POST['superpower'];
$comment=$_POST['comment'];
$confirm=$_POST['confirm'];


$user = 'u24108';
$pass = '3343567';
$db = new PDO('mysql:host=localhost;dbname=u24108', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


try {
  $stmt = $db->prepare("INSERT INTO forma (lastname, name, email, date, sex, kolvo, superpower, comment, check2) VALUE (:lastname,:name, :email,:date,:sex,:kolvo,:superpower,:comment,:check2)");
  $stmt -> execute(['lastname'=>$lastname,'name'=>$name,'email'=>$email,'date'=>$date,'sex'=>$sex,'kolvo'=>$kolvo,'superpower'=>$superpower,'comment'=>$comment,'check2'=>$confirm]);
  echo "<script type='text/javascript'>alert('Спасибо, результаты сохранены.');</script>";

}

catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

setcookie('save', '1');

header('Location: index.php');

}


